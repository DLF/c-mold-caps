# Terminal C Molded Caps

Work in progress.

CadQuery code that produces 3D-printalble caps and cap-molds for [Terminal C](https://gitlab.com/DLF/terminal-c).

# Notes on Materials for the Molding
* Silicone for a finger-cap mold (both parts): ca. 24ml IFace + 39ml Cap (43 for row 2) = 63 ml (67 ml)
  (This includes sufficient buffer.)
* Silicone for upper-thumb mold: ca. 30ml IFace + 55ml Cap = 85ml
* Bounding-Box for r0,1: 18.5 * 18.5 * 11 = 3.75 ml
* Bounding-Box for r2: 18.5 * 18.5 * 13= 4.5 ml
* Bounding-Box for thumb-upper: 20.5 * 22 * 11 = 5 ml
* Bounding-Box for thumb-lower: 26 * 18.5 * 8.5 = 4 ml

* One complete set (1×TL, all others ×2) is 36 ml (12 + 24) (11 + 22)
* 3 + 3 + 3 + 2 + 1: [33 + 7 + 4.5  =  44.5]  42  (14 + 28) (13 + 26)

# Notes on a Geometry Revision
* Must: Increase Distance from controller area to most inner row. It's too tight for the cap.
* Can: Get the most inner screw (on the thumb cluster out of the way of the cap. Maybe move it down or make the side part bigger...)

