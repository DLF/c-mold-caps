from c_moldcaps.cap.std_cap import (StdCapShell, StdCapShellConfig)
from c_moldcaps.cap.thumb_upper_cap import ThumbUpperCapShell, ThumbUpperCapShellConfig
from c_moldcaps.cap.thumb_lower_cap import ThumbLowerCapShell, ThumbLowerCapShellConfig
from c_moldcaps.cap.mx_iface import (MXIFace, MXIFaceConfig)
from c_moldcaps.cap import Cap
from c_moldcaps.mold import (Mold, MoldConfig)

import cadquery as cq

from c_moldcaps.mold_collection import MoldCollection

mc = MoldCollection()

if False:
    finger_y_translation = 90
    cap_row0 = mc.row0_mold.cap.stem_on_ground.translate(cq.Vector(0, finger_y_translation))
    cap_row1 = mc.row1_mold.cap.stem_on_ground.translate(cq.Vector(0, 19.05, 0)).translate(cq.Vector(0, finger_y_translation))
    cap_row2 = mc.row2_mold.cap.stem_on_ground.translate(cq.Vector(0, 2 * 19.05, 0)).translate(cq.Vector(0, finger_y_translation))


if False:
    lower_cap = mc.thumb_lower_mold.cap
    upper_cap = mc.thumb_upper_mold.cap
    lower_shell = lower_cap.shell
    upper_shell = upper_cap.shell

    uppers = [
        upper_cap.stem_on_ground.translate((0, 67, 0)).rotate((0, 0, -1), (0, 0, 1), 0 - n * 17)
        for n in range(4)
    ]
    uppers = uppers[0] + uppers[1] + uppers[2] + uppers[3]
    lowers = [
        lower_cap.stem_on_ground.translate((0, 45, 0)).rotate((0, 0, -1), (0, 0, 1), -17/2 - n * 17 * 2)
        for n in range(2)
    ]
    lowers = lowers[0] + lowers[1]


box1 = mc.thumb_lower_mold.mold_shell + mc.thumb_lower_mold.mold_shell_box
