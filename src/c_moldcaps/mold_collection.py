import os
import pathlib
from typing import Optional

from c_moldcaps.cap.std_cap import StdCapShell, StdCapShellConfig
from c_moldcaps.cap.thumb_upper_cap import ThumbUpperCapShell, ThumbUpperCapShellConfig
from c_moldcaps.cap.thumb_lower_cap import ThumbLowerCapShell, ThumbLowerCapShellConfig
from c_moldcaps.cap.mx_iface import MXIFaceConfig, MXIFace
from c_moldcaps.cap import Cap
from mold import MoldConfig, Mold
import cadquery as cq


def get_output_base_dir():
    self_dir_str = os.path.dirname(__file__)
    out_dir_str = os.path.join(self_dir_str, "..", "..", "build")
    out_path = pathlib.Path(out_dir_str).resolve()
    if not os.path.exists(out_path):
        os.makedirs(out_path)
    return out_path


class MoldCollection:
    row0_config = StdCapShellConfig(
        top_slope_degrees=0,
        height=10.0
    )
    row1_config = StdCapShellConfig(
        top_slope_degrees=6,
        height=10.6
    )
    row2_config = StdCapShellConfig(
        top_slope_degrees=12,
        height=12.8
    )

    def __init__(self):

        self.mold_config = MoldConfig()
        self.iface_config = MXIFaceConfig()

        self.iface = MXIFace(self.iface_config)

        self.row0_mold = Mold(
            config=self.mold_config,
            cap=Cap(
                shell=StdCapShell(MoldCollection.row0_config),
                iface=self.iface,
            )
        )
        self.row1_mold = Mold(
            config=self.mold_config,
            cap=Cap(
                shell=StdCapShell(config=MoldCollection.row1_config),
                iface=self.iface,
            )
        )
        self.row2_mold = Mold(
            config=self.mold_config,
            cap=Cap(
                shell=StdCapShell(config=MoldCollection.row2_config),
                iface=self.iface,
            )
        )
        self.thumb_upper_mold = Mold(
            config=self.mold_config,
            cap=Cap(
                shell=ThumbUpperCapShell(config=ThumbUpperCapShellConfig()),
                iface=self.iface,
            )
        )
        self.thumb_lower_mold = Mold(
            config=self.mold_config,
            cap=Cap(
                shell=ThumbLowerCapShell(config=ThumbLowerCapShellConfig()),
                iface=self.iface,
            )
        )

        self.mold_by_name = {
            "row0": self.row0_mold,
            "row1": self.row1_mold,
            "row2": self.row2_mold,
            "thumb-upper": self.thumb_upper_mold,
            "thumb-lower": self.thumb_lower_mold,
        }

    def render(self, cap_only: Optional[str] = None):
        output_format = "stl"
        tolerance = 1e-4
        base_dir = get_output_base_dir()
        for cap_name, mold in self.mold_by_name.items():
            if cap_only is not None and cap_name != cap_only:
                continue
            print("\n## Cap \"{}\"".format(cap_name))
            cap_dir = os.path.join(base_dir, cap_name)
            if not os.path.exists(cap_dir):
                os.makedirs(cap_dir)
            for part_name, part in [
                ("mold-shell", mold.mold_shell),
                ("mold-shell-box", mold.mold_shell_box),
                ("mold-iface_no-stem", mold.mold_iface_no_stem),
                ("mold-iface_with-stem", mold.mold_iface_with_stem),
                ("mold-iface-box", mold.mold_iface_box),
                ("mold-iface-tray", mold.mold_iface_tray.rotate((0, 0, 0), (1, 0, 0), 180)),
                ("test_solid-cap", mold.cap.shell.solid),
                ("cap", mold.cap.cap_on_ground)
            ]:
                print("  Rendering part \"{}\"...".format(part_name))
                file_name = "{cap}_{part}.{format}".format(
                    cap=cap_name,
                    part=part_name,
                    format=output_format
                )
                out_file_path = os.path.join(cap_dir, file_name)
                cq.exporters.export(part, out_file_path, tolerance=tolerance)


if __name__ == "__main__":
    collection = MoldCollection()
    collection.render()
    #collection.render("thumb-upper")
