from dataclasses import dataclass
import cadquery as cq

from c_moldcaps.cap import Interface


@dataclass
class MXIFaceConfig:
    stem_circle_radius = 2.75
    stem_height = 4.4
    cutout_width_bottom = 15.2
    cutout_width_top = 11.50
    cutout_fillet = 0.5
    # support bar
    x_bar = 6
    y_bar = 3.2
    bar_rounding = 0.25
    # cross
    s_x1 = 1.35
    s_y1 = 4.2
    s_x2 = 4.2
    s_y2 = 1.42
    corner_rounding_radius = 0.1
    corner_rounding_offset = 0.07
    inner_rounding_radius = 0.3


class MXIFace(Interface):

    @property
    def cutout_z_height(self) -> float:
        return self.c.stem_height

    @property
    def cutout_x_width(self) -> float:
        return self.c.cutout_width_bottom

    @property
    def cutout_y_width(self) -> float:
        return self.c.cutout_width_bottom

    def __init__(self, config: MXIFaceConfig):
        self.c = config

    @property
    def cutout(self) -> cq.Workplane:
        wb = self.c.cutout_width_bottom
        wt = self.c.cutout_width_top
        h = self.c.stem_height
        f = self.c.cutout_fillet
        bottom = cq.Sketch().rect(wb, wb).vertices().fillet(f)
        top = cq.Sketch().rect(wt, wt).vertices().fillet(f)
        return (
                cq.Workplane()
                .placeSketch(
                    bottom,
                    top.moved(cq.Location(cq.Vector((0, 0, h))))
                )
                .loft()
                +
                cq.Workplane()
                .placeSketch(
                    bottom.moved(cq.Location(cq.Vector((0, 0, -10)))),
                    bottom,
                )
                .loft()
        )

    @property
    def stem(self) -> cq.Workplane:
        circle_indent = self.c.corner_rounding_offset
        y1c = self.c.s_y1 / 2 - circle_indent
        x1c = self.c.s_x1 / 2 - circle_indent
        y2c = self.c.s_y2 / 2 - circle_indent
        x2c = self.c.s_x2 / 2 - circle_indent
        use_bar = self.c.x_bar is not None and self.c.y_bar is not None
        sketch = cq.Sketch()
        if use_bar:
            sketch.rect(self.c.x_bar, self.c.y_bar)
        sketch = (
            sketch
            .circle(r=self.c.stem_circle_radius)
            .rect(self.c.s_x1, self.c.s_y1, mode="s")
            .rect(self.c.s_x2, self.c.s_y2, mode="s")
            .reset()
            .rect(self.c.s_x1, self.c.s_y1, mode="s")
            .rect(self.c.s_x2, self.c.s_y2, mode="s")
            .vertices(cq.NearestToPointSelector((0.5, 0.5)))
            .fillet(self.c.inner_rounding_radius)
            .reset()
            .vertices(cq.NearestToPointSelector((0.5, -0.5)))
            .fillet(self.c.inner_rounding_radius)
            .reset()
            .vertices(cq.NearestToPointSelector((-0.5, -0.5)))
            .fillet(self.c.inner_rounding_radius)
            .reset()
            .vertices(cq.NearestToPointSelector((-0.5, 0.5)))
            .fillet(self.c.inner_rounding_radius)
            .reset()
            .push([
                (-x1c, -y1c),
                (-x1c, y1c),
                (x1c, -y1c),
                (x1c, y1c),
                (-x2c, -y2c),
                (-x2c, y2c),
                (x2c, -y2c),
                (x2c, y2c),
            ])
            .circle(r=self.c.corner_rounding_radius, mode="s")
            .reset()
        )
        if use_bar:
            dx = self.c.x_bar / 2
            dy = self.c.y_bar / 2
            sketch = (
                sketch
                .vertices(cq.NearestToPointSelector((dx, dy)))
                .fillet(self.c.bar_rounding)
                .reset()
                .vertices(cq.NearestToPointSelector((dx, -dy)))
                .fillet(self.c.bar_rounding)
                .reset()
                .vertices(cq.NearestToPointSelector((-dx, dy)))
                .fillet(self.c.bar_rounding)
                .reset()
                .vertices(cq.NearestToPointSelector((-dx, -dy)))
                .fillet(self.c.bar_rounding)
                .reset()
            )
        return (
                cq.Workplane().placeSketch(sketch).extrude(self.c.stem_height)
        )
