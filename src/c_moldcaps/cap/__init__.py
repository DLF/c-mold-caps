import cadquery as cq

# switch is 6.6mm above plate (without stem)
# stem is 3.6mm


class CapShell:
    @property
    def x_width(self) -> float:
        raise NotImplementedError()

    @property
    def y_width(self) -> float:
        raise NotImplementedError()

    @property
    def z_height(self) -> float:
        raise NotImplementedError()

    @property
    def solid(self) -> cq.Workplane:
        raise NotImplementedError()

    @property
    def interface_indentation(self) -> float:
        raise NotImplementedError()


class Interface:
    @property
    def stem(self) -> cq.Workplane:
        raise NotImplementedError()

    @property
    def cutout(self) -> cq.Workplane:
        raise NotImplementedError()

    @property
    def cutout_x_width(self) -> float:
        raise NotImplementedError()

    @property
    def cutout_y_width(self) -> float:
        raise NotImplementedError()

    @property
    def cutout_z_height(self) -> float:
        raise NotImplementedError()


class Cap:
    def __init__(self, shell: CapShell, iface: Interface):
        self.shell = shell
        self.iface = iface

    @property
    def stem_on_ground(self) -> cq.Workplane:
        return (
                self.shell.solid.translate(cq.Vector(0, 0, -self.shell.interface_indentation))
                -
                self.iface.cutout
                +
                self.iface.stem
        )

    @property
    def cap_on_ground(self) -> cq.Workplane:
        return self.stem_on_ground.translate(cq.Vector(0, 0, self.shell.interface_indentation))
