import cadquery as cq
from dataclasses import dataclass

from c_moldcaps.cap import CapShell
from c_moldcaps.cap.radial_base import RadialBase


@dataclass
class ThumbUpperCapShellConfig:
    def __init__(
        self,
        side_angle: float = 8.5,
        side_distance: float = 0.4,
        radius: float = 67,
        upper_length: float = 10,
        lower_length: float = 11.6,
        height_below_stemground: float = 2.5,
        height: float = 13.5,
        lower_base_height: float = 1.8,
        indent: float = 5,
        top_edge_fillet: float = 2.0,
        base_fillet: float = 2,
        base_fillet_upside: float = 4,
        # middle
        middle_sketch_z: float = 2,
        middle_sketch_radius: float = 67 - 1.2,
        middle_sketch_upper_length: float = 10 - 0.2,
        middle_sketch_lower_length: float = 11.6 - 0.2,
        middle_sketch_side_angle_reduction: float = 0.001,
        middle_sketch_fillet: float = 2.2,
        middle_sketch_fillet_upside: float = 4,
        # upper
        upper_sketch_radius: float = 67 - 3,
        upper_sketch_upper_length: float = 10 - 3.5,
        upper_sketch_lower_length: float = 11.6 - 3,
        upper_sketch_side_angle_reduction: float = 3,
        upper_sketch_fillet: float = 3,
        upper_sketch_fillet_upside: float = 1,
    ):
        self.height_below_stemground = height_below_stemground
        self.height = height
        self.base_fillet = base_fillet
        self.base_fillet_upside = base_fillet_upside
        self.lower_base_height = lower_base_height
        self.side_angle = side_angle
        self.side_distance = side_distance
        self.radius = radius
        self.upper_length = upper_length
        self.lower_length = lower_length
        self.indent = indent
        self.top_edge_fillet = top_edge_fillet
        self.middle_sketch_z = middle_sketch_z
        self.middle_sketch_radius = middle_sketch_radius
        self.middle_sketch_upper_length = middle_sketch_upper_length
        self.middle_sketch_lower_length = middle_sketch_lower_length
        self.middle_sketch_side_angle_reduction = middle_sketch_side_angle_reduction
        self.middle_sketch_fillet = middle_sketch_fillet
        self.middle_sketch_fillet_upside = middle_sketch_fillet_upside
        self.upper_sketch_radius = upper_sketch_radius
        self.upper_sketch_upper_length = upper_sketch_upper_length
        self.upper_sketch_lower_length = upper_sketch_lower_length
        self.upper_sketch_side_angle_reduction = upper_sketch_side_angle_reduction
        self.upper_sketch_fillet = upper_sketch_fillet
        self.upper_sketch_fillet_upside = upper_sketch_fillet_upside


class ThumbUpperCapShell(CapShell, RadialBase):

    @property
    def z_height(self) -> float:
        return self.c.height

    def __init__(self, config: ThumbUpperCapShellConfig):
        self.c = config
        print("x-width: {}".format(self.x_width))

    @property
    def outer_radius(self) -> float:
        return float(self.c.radius + self.c.upper_length)

    @property
    def inner_radius(self):
        return self.c.radius + self.c.lower_length

    @property
    def x_width(self) -> float:
        x, y = self.radial_point(
            self.c.radius + self.c.upper_length,
            self.c.side_angle
        )
        return 2 * x

    @property
    def y_width(self) -> float:
        lower_corner = self.radial_point(radius=self.c.radius - self.c.lower_length, angle=self.c.side_angle)
        length = self.c.radius + self.c.upper_length - lower_corner[1]
        return length

    @property
    def interface_indentation(self) -> float:
        return self.c.height_below_stemground

    @property
    def _sketch1(self) -> cq.Sketch:
        return self._get_radial_base_sketch(
            lower_radius=self.c.radius - self.c.lower_length,
            middle_radius=self.c.radius,
            upper_radius=self.c.radius + self.c.upper_length,
            side_angle=self.c.side_angle,
            fillet=self.c.base_fillet,
            upper_fillet=self.c.base_fillet_upside,
            side_distance=self.c.side_distance,
        )

    @property
    def _sketch2(self) -> cq.Sketch:
        return self._get_radial_base_sketch(
            lower_radius=self.c.middle_sketch_radius - self.c.middle_sketch_lower_length,
            middle_radius=self.c.middle_sketch_radius,
            upper_radius=self.c.middle_sketch_radius + self.c.middle_sketch_upper_length,
            side_angle=self.c.side_angle - self.c.middle_sketch_side_angle_reduction,
            fillet=self.c.middle_sketch_fillet,
            upper_fillet=self.c.middle_sketch_fillet_upside,
            side_distance=self.c.side_distance,
        )

    @property
    def _sketch3(self) -> cq.Sketch:
        return self._get_radial_base_sketch(
            lower_radius=self.c.upper_sketch_radius - self.c.upper_sketch_lower_length,
            middle_radius=self.c.upper_sketch_radius,
            upper_radius=self.c.upper_sketch_radius + self.c.upper_sketch_upper_length,
            side_angle=self.c.side_angle - self.c.upper_sketch_side_angle_reduction,
            fillet=self.c.upper_sketch_fillet,
            upper_fillet=self.c.upper_sketch_fillet_upside,
            side_distance=self.c.side_distance,
        )

    @property
    def _top_cutout(self) -> cq.Workplane:
        upper_corner = self.radial_point(radius=self.c.radius + self.c.upper_length, angle=self.c.side_angle)
        lower_corner = self.radial_point(radius=self.c.radius - self.c.lower_length, angle=self.c.side_angle)
        radius_upper = upper_corner[0]
        radius_lower = lower_corner[0]
        indent = self.c.indent
        length = self.c.radius + self.c.upper_length - lower_corner[1]
        return (
            cq.Workplane()
            .placeSketch(
                cq.Sketch().ellipse(radius_lower, indent),
                cq.Sketch().ellipse(radius_upper, indent).moved(cq.Location(cq.Vector(0, 0, length)))
            )
            .loft()
            .rotate(axisStartPoint=(-1, 0), axisEndPoint=(1, 0), angleDegrees=-90)
            .translate(cq.Vector(
                0,
                -(self.c.radius - lower_corner[1]),
                indent
            ))
            .translate(cq.Vector(
                0,
                0,
                self.c.height - self.c.indent
            ))
        )

    @property
    def solid(self) -> cq.Workplane:
        return (
            (
                cq.Workplane()
                .placeSketch(
                    self._sketch1,
                    self._sketch2.moved(cq.Location(cq.Vector(0, 0, self.c.middle_sketch_z))),
                    self._sketch3.moved(cq.Location(cq.Vector(0, 0, self.c.height - self.c.lower_base_height)))
                )
                .loft()
                -
                self._top_cutout
            )
            .edges(">Z")
            .fillet(self.c.top_edge_fillet)
            .translate(cq.Vector(0, 0, self.c.lower_base_height))
            +
            (
                cq.Workplane()
                .placeSketch(
                    self._sketch1
                )
                .extrude(self.c.lower_base_height)
            )
        )


