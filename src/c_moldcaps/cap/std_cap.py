import cadquery as cq
from dataclasses import dataclass
from c_moldcaps.cap import CapShell


@dataclass
class StdCapShellConfig:

    def __init__(
        self,
        width_base: float = 18.0,
        width_top: float = 13.0,
        height: float = 11.0,
        height_below_stemground: float = 2.5,
        base_fillet: float = 1.5,
        base_height: float = 1,
        top_fillet: float = 5,
        top_slope_degrees: float = 7,
        top_curv: float = 1.7,
        top_edge_fillet: float = 1.2,
        scoop_depth: float = 1.8,
        middle_sketch_size_factor: float = 0.975,
        middle_fillet: float = 1.9,

    ):
        self.width_base = width_base
        self.width_top = width_top
        self.height = height
        self.height_below_stemground = height_below_stemground
        self.base_fillet = base_fillet
        self.base_height = base_height
        self.top_fillet = top_fillet
        self.top_slope_degrees = top_slope_degrees
        self.top_curv = top_curv
        self.top_edge_fillet = top_edge_fillet
        self.scoop_depth = scoop_depth
        self.middle_sketch_size_factor = middle_sketch_size_factor
        self.mid_fillet = middle_fillet

        assert self.width_base > self.width_top

        self.tension = 1

        self.width_diff = self.width_base - self.width_top

        self.width_x = self.width_base
        self.width_y = self.width_base

        self.top_diff = self.width_base - self.width_top


class StdCapShell(CapShell):

    @property
    def z_height(self) -> float:
        return self.c.height

    def __init__(self, config: StdCapShellConfig):
        self.c = config

    @property
    def base(self) -> cq.Sketch:
        return cq.Sketch()\
            .rect(self.c.width_x, self.c.width_y).vertices().fillet(self.c.base_fillet)

    @property
    def mid(self) -> cq.Sketch:
        return (
            cq.Sketch()
            .rect(
                self.c.middle_sketch_size_factor * self.c.width_x,
                self.c.middle_sketch_size_factor * self.c.width_y
            ).vertices().fillet(self.c.mid_fillet)
        )

    @property
    def top(self) -> cq.Sketch:
        c = self.c
        tx = c.width_x - c.top_diff
        ty = c.width_y - c.top_diff
        curv = c.top_curv
        tension = c.tension
        return (
            cq.Sketch()
            .arc((curv, curv * tension), (0, ty / 2), (curv, ty - curv * tension))
            .arc((curv, ty - curv * tension), (tx / 2, ty), (tx - curv, ty - curv * tension))
            .arc((tx - curv, ty - curv * tension), (tx, ty / 2), (tx - curv, curv * tension))
            .arc((tx - curv, curv * tension), (tx / 2, 0), (curv, curv * tension))
            .assemble()
            .vertices()
            .fillet(c.top_fillet)
            .moved(cq.Location(cq.Vector(-tx/2, -tx/2, 0)))
        )

    @property
    def convex_top_cutout(self) -> cq.Workplane:
        by = self.c.width_y
        bx = self.c.width_x
        return (
            cq.Workplane("YZ").transformed(
                offset=cq.Vector(0, self.c.height-2.1, -bx/2),
                rotate=cq.Vector(0, 0, self.c.top_slope_degrees)
            )
            .moveTo(-by/2, -1)
            .threePointArc((0, 2), (by/2, -1))
            .lineTo(by/2, 10)
            .lineTo(-by/2, 10)
            .close()
            .extrude(bx, combine=False)
        )

    @property
    def concave_top_cutout(self) -> cq.Workplane:
        by = self.c.width_y
        bx = self.c.width_x
        height = self.c.height
        angle = self.c.top_slope_degrees
        depth = self.c.scoop_depth
        return (
            cq.Workplane("YZ").transformed(offset=cq.Vector(0, height, bx/2), rotate=cq.Vector(0, 0, angle))
            .moveTo(-by/2+2,0)
            .threePointArc((0, min(-0.1, -depth+1.5)), (by/2-2, 0))
            .lineTo(by/2, height)
            .lineTo(-by/2, height)
            .close()
            .workplane(offset=-bx/2)
            .moveTo(-by/2-2, -0.5)
            .threePointArc((0, -depth), (by/2+2, -0.5))
            .lineTo(by/2, height)
            .lineTo(-by/2, height)
            .close()
            .workplane(offset=-bx/2)
            .moveTo(-by/2+2, 0)
            .threePointArc((0, min(-0.1, -depth+1.5)), (by/2-2, 0))
            .lineTo(by/2, height)
            .lineTo(-by/2, height)
            .close()
            .loft(combine=False)
        )

    @property
    def solid(self) -> cq.Workplane:
        upper_height = self.c.height - self.c.base_height
        raw = (
            cq.Workplane("XY").placeSketch(
                self.base,
                self.mid.moved(cq.Location(cq.Vector(0, 0, upper_height/4), (1, 0, 0), self.c.top_slope_degrees/4)),
                self.top.moved(cq.Location(cq.Vector(0, 0, upper_height), (1, 0, 0), self.c.top_slope_degrees))
            )
            .loft()
        )
        return (
            (raw - self.concave_top_cutout)
            .edges(">Z")
            .fillet(self.c.top_edge_fillet)
            .translate(cq.Vector(0, 0, self.c.base_height))
            +
            (
                cq.Workplane()
                .placeSketch(
                    self.base
                )
                .extrude(self.c.base_height)
            )
        )

    @property
    def interface_indentation(self) -> float:
        return self.c.height_below_stemground

    @property
    def x_width(self) -> float:
        return self.c.width_x

    @property
    def y_width(self) -> float:
        return self.c.width_y

