from typing import Optional

import cadquery as cq
import math


class RadialBase:
    @staticmethod
    def radial_point(radius: float, angle: float) -> tuple[float, float]:
        x = math.sin(math.radians(angle)) * radius
        y = math.cos(math.radians(angle)) * radius
        return x, y

    def _get_radial_base_sketch(
            self,
            lower_radius: float,
            middle_radius: float,
            upper_radius: float,
            side_angle: float,
            fillet: float,
            side_distance: float,
            upper_fillet: Optional[float] = None,
    ) -> cq.Sketch:
        if upper_fillet is None:
            upper_fillet = fillet
        r1 = lower_radius
        r2 = upper_radius
        dy_pie = side_distance / math.sin(math.radians(side_angle))
        p_pie_r = self.radial_point(
            radius=2 * upper_radius,
            angle=side_angle
        )
        p_pie_r = p_pie_r[0], p_pie_r[1] + dy_pie
        p_pie_l = -p_pie_r[0], p_pie_r[1]

        fillet_anchor_nw = self.radial_point(r2, -side_angle)
        fillet_anchor_ne = self.radial_point(r2, +side_angle)
        fillet_anchor_sw = self.radial_point(r1, -side_angle)
        fillet_anchor_se = self.radial_point(r1, +side_angle)

        base: cq.Sketch = (
            cq.Sketch()
            .circle(r2)
            .circle(r1, mode="s")
            .segment((0, dy_pie), p_pie_l)
            .segment(p_pie_r)
            .close()
            .assemble(mode="i")
        )
        base: cq.Sketch = (
            base
            .vertices(cq.NearestToPointSelector(fillet_anchor_ne))
            .fillet(upper_fillet)
            .reset()
            .vertices(cq.NearestToPointSelector(fillet_anchor_nw))
            .fillet(upper_fillet)
            .reset()
            .vertices(cq.NearestToPointSelector(fillet_anchor_se))
            .fillet(fillet)
            .reset()
            .vertices(cq.NearestToPointSelector(fillet_anchor_sw))
            .fillet(fillet)
            .reset()
        )
        return base.moved(cq.Location(cq.Vector(0, -middle_radius)))


