import cadquery as cq
from dataclasses import dataclass

from c_moldcaps.cap import CapShell
from c_moldcaps.cap.radial_base import RadialBase


@dataclass
class ThumbLowerCapShellConfig:
    def __init__(
        self,
        side_angle: float = 17,
        side_distance: float = 0.4,
        radius: float = 45,
        upper_length: float = 9.4,
        lower_length: float = 9,
        height_below_stemground: float = 2.5,
        height: float = 8.9,
        base_fillet: float = 2,
        lower_base_height: float = 2.5,
        middle_sketch_z: float = 2.4,
        middle_sketch_upper_length: float = 8.95,
        middle_sketch_lower_length: float = 8.7,
        middle_sketch_side_angle_reduction: float = 0.35,
        middle_sketch_fillet: float = 3,
        upper_sketch_upper_length: float = 0.1,
        upper_sketch_lower_length: float = 0.1,
        upper_sketch_side_angle_reduction: float = 6,
        upper_sketch_fillet: float = 0.01,
    ):
        self.side_angle = side_angle
        self.side_distance = side_distance
        self.radius = radius
        self.upper_length = upper_length
        self.lower_length = lower_length
        self.height_below_stemground = height_below_stemground
        self.height = height
        self.base_fillet = base_fillet
        self.lower_base_height = lower_base_height
        self.middle_sketch_z = middle_sketch_z
        self.middle_sketch_upper_length = middle_sketch_upper_length
        self.middle_sketch_lower_length = middle_sketch_lower_length
        self.middle_sketch_fillet = middle_sketch_fillet
        self.middle_sketch_side_angle_reduction = middle_sketch_side_angle_reduction
        self.upper_sketch_upper_length = upper_sketch_upper_length
        self.upper_sketch_lower_length = upper_sketch_lower_length
        self.upper_sketch_side_angle_reduction = upper_sketch_side_angle_reduction
        self.upper_sketch_fillet = upper_sketch_fillet


class ThumbLowerCapShell(CapShell, RadialBase):

    @property
    def z_height(self) -> float:
        return self.c.height

    def __init__(self, config: ThumbLowerCapShellConfig):
        self.c = config

    @property
    def outer_radius(self) -> float:
        return float(self.c.radius + self.c.upper_length)

    @property
    def inner_radius(self):
        return self.c.radius + self.c.lower_length

    @property
    def x_width(self) -> float:
        x, y = self.radial_point(
            self.c.radius + self.c.upper_length,
            self.c.side_angle
        )
        return 2 * x

    @property
    def y_width(self) -> float:
        lower_corner = self.radial_point(radius=self.c.radius - self.c.lower_length, angle=self.c.side_angle)
        length = self.c.radius + self.c.upper_length - lower_corner[1]
        return length

    @property
    def interface_indentation(self) -> float:
        return self.c.height_below_stemground

    @property
    def _sketch1(self) -> cq.Sketch:
        return self._get_radial_base_sketch(
            lower_radius=self.c.radius - self.c.lower_length,
            middle_radius=self.c.radius,
            upper_radius=self.c.radius + self.c.upper_length,
            side_angle=self.c.side_angle,
            fillet=self.c.base_fillet,
            side_distance=self.c.side_distance,
        )

    @property
    def _sketch2(self) -> cq.Sketch:
        return self._get_radial_base_sketch(
            lower_radius=self.c.radius - self.c.middle_sketch_lower_length,
            middle_radius=self.c.radius,
            upper_radius=self.c.radius + self.c.middle_sketch_upper_length,
            side_angle=self.c.side_angle - self.c.middle_sketch_side_angle_reduction,
            fillet=self.c.middle_sketch_fillet,
            side_distance=self.c.side_distance,
        )

    @property
    def _sketch3(self) -> cq.Sketch:
        return self._get_radial_base_sketch(
            lower_radius=self.c.radius - self.c.upper_sketch_lower_length,
            middle_radius=self.c.radius,
            upper_radius=self.c.radius + self.c.upper_sketch_upper_length,
            side_angle=self.c.side_angle - self.c.upper_sketch_side_angle_reduction,
            fillet=self.c.upper_sketch_fillet,
            side_distance = self.c.side_distance,
        )

    @property
    def solid(self) -> cq.Workplane:
        return (
                (
                    cq.Workplane()
                    .placeSketch(
                        self._sketch1,
                        self._sketch2.moved(cq.Location(cq.Vector(0, 0, self.c.middle_sketch_z))),
                        self._sketch3.moved(cq.Location(cq.Vector(0, 0, self.c.height - self.c.lower_base_height)))
                    )
                    .loft()
                )
                .translate(cq.Vector(0, 0, self.c.lower_base_height))
                +
                (
                    cq.Workplane()
                    .placeSketch(
                        self._sketch1
                    )
                    .extrude(self.c.lower_base_height)
                )
        )
