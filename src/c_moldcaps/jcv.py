from jupyter_cadquery.viewer.client import show

from src.c_moldcaps.cap.std_cap import StdCapShell, StdCapShellConfig

import cadquery as cq

m = StdCapShell(StdCapShellConfig())()

show(m)

cq.exporters.export(m, "cap.amf", tolerance=1e-5)
cq.exporters.export(m, "cap.stl", tolerance=1e-5)


