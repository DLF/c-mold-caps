import cadquery as cq
from dataclasses import dataclass
from c_moldcaps.cap import Cap


@dataclass
class MoldConfig:
    # height of the base plate for the shell (=cap) mold
    shell_base_height = 2.4
    # height of the base plate for the iface (=back side) mold (= thickness of the block beneath iface cutout)
    iface_base_height = 2
    # width between the keys and the wall
    base_extra_width = 3.0
    # radius of the base rounding
    base_fillet = 6
    # thickness of the box walls
    box_wall_width = 2
    # clearance between the mold blocks and the box *per side* (overall clearance is x 2)
    # the clearance is added to the wall
    box_clearance = 0.16
    # height of the shell-mold-box above the bounding box of the shell
    shell_box_extra_height = 8
    # distance between the keys and outer shell of the cap (according to the caps bounding width)
    key_distance = 4.8
    # height of the keys
    key_height = 2
    # diameter of the round keys and depth of the rectangular key
    key_width = 4.4
    # vertical clearance for all keys (hight clearance does not harm)
    key_vertical_clearance = 1  # cap-part is enlarged (the cap mold has holes which are bigger by this than the pins)
    # horizontal clearance of all keys on all sides (overall clearance is x 2 in all directions)
    key_horizontal_clearance = 0.08
    # length of the rectangular key
    key_rect_length = 8
    # rounding radius on the top of the keys
    key_top_fillet = 1
    # rounding of the vertical edges of the rectangular key
    key_rect_side_fillet = 1.5
    # the diameter of the channels
    vertical_channel_diameter = 1.2 + 0.6
    # the distance from the channel to the iface-bounding rect
    vertical_channel_distance = 0.5
    # the depth of the channel holes in the iface-mold
    vertical_channel_puncture_depth = 5  # how deep the hole for the channel part is
    # extra space on each side of the tray, whose base size is the size of the cutout
    tray_over_size = 2.4
    # extra space on each side of the bigger top
    tray_top_extra_size = 2
    # distance between tray and cap bottom level (thickness of mold part)
    tray_distance = 5
    # height of the tray, intended to be inside the mold
    tray_height = 2
    # height of the tray above the intended mold part
    tray_overheight = 4
    # side (vertical) fillet of the tray (same on all levels)
    tray_fillet = 3


class Mold:
    def __init__(self, config: MoldConfig, cap: Cap):
        self.c = config
        self.cap = cap
        self.iface = cap.iface
        self.block_height = (
            self.c.iface_base_height
            +
            self.cap.shell.interface_indentation
            +
            self.cap.iface.cutout_z_height
        )

    @property
    def _key_coords(self) -> list[tuple[float, float]]:
        dx = (
                self.cap.shell.x_width / 2.0
                +
                self.c.key_distance
                +
                self.c.key_width / 2.0
        )
        dy = (
                self.cap.shell.y_width / 2.0
                +
                self.c.key_distance
                +
                self.c.key_width / 2.0
        )
        return [
            (0, dy),
            (dx, dy),
            (dx, 0),
            (dx, -dy),
            (0, -dy),
            (-dx, -dy),
            (-dx, 0),
            (-dx, dy)
        ]

    @property
    def _round_key_coords(self) -> list[tuple[float, float]]:
        return self._key_coords[1:]

    @property
    def _rec_key_coord(self) -> tuple[float, float]:
        return self._key_coords[0]

    def __get_round_keys(self, enlarged_by_clearance: bool) -> cq.Workplane:
        v_clear = self.c.key_vertical_clearance if enlarged_by_clearance else 0
        h_clear = self.c.key_horizontal_clearance if enlarged_by_clearance else 0
        return (
            cq.Workplane()
            .pushPoints(self._round_key_coords)
            .circle(radius=self.c.key_width / 2 + h_clear)
            .extrude(self.c.key_height + v_clear)
            .faces(">Z")
            .fillet(self.c.key_top_fillet)
        )

    def __get_rec_key(self, enlarged_by_clearance: bool) -> cq.Workplane:
        c = self._rec_key_coord
        v_clear = self.c.key_vertical_clearance if enlarged_by_clearance else 0
        h_clear = self.c.key_horizontal_clearance if enlarged_by_clearance else 0
        return (
            cq.Workplane()
            .center(
                x=c[0],
                y=c[1]
            )
            .rect(xLen=self.c.key_rect_length + 2 * h_clear, yLen=self.c.key_width + 2 * h_clear)
            .extrude(self.c.key_height + v_clear)
            .edges("|Z")
            .fillet(self.c.key_rect_side_fillet + h_clear)
            .edges(">Z")
            .fillet(self.c.key_top_fillet)
        )

    def _get_keys(self, enlarged_by_clearance: bool) -> cq.Workplane:
        return (
            self.__get_round_keys(enlarged_by_clearance)
            +
            self.__get_rec_key(enlarged_by_clearance)
        )

    @property
    def base_dim(self) -> tuple[float, float]:
        x_len = (
                self.cap.shell.x_width
                +
                self.c.key_distance * 2
                +
                self.c.key_width * 2
                +
                self.c.base_extra_width * 2

        )
        y_len = (
                self.cap.shell.y_width
                +
                self.c.key_distance * 2
                +
                self.c.key_width * 2
                +
                self.c.base_extra_width * 2
        )
        return x_len, y_len

    @property
    def _channel_beams(self) -> cq.Workplane:
        dx = (
            self.iface.cutout_x_width / 2
            +
            self.c.vertical_channel_diameter / 2
            +
            self.c.vertical_channel_distance
        )
        dy = (
                self.iface.cutout_y_width / 2
                +
                self.c.vertical_channel_diameter / 2
                +
                self.c.vertical_channel_distance
        )
        x = dx / 2
        y = dy / 2
        locations = [
            (-x, dy),
            (x, dy),
            (dx, y),
            (dx, -y),
            (-x, -dy),
            (x, -dy),
            (-dx, y),
            (-dx, -y),
        ]
        z_translation = self.block_height - self.c.vertical_channel_puncture_depth
        return (
            cq.Workplane()
            .pushPoints(locations)
            .circle(radius=self.c.vertical_channel_diameter / 2)
            .extrude(50)
            .translate(cq.Vector(0, 0, z_translation))
        )

    def _get_base_block(self, height: float) -> cq.Workplane:
        x_len, y_len = self.base_dim
        return (
            cq.Workplane()
            .rect(xLen=x_len, yLen=y_len)
            .extrude(height)
            .edges("|Z")
            .fillet(self.c.base_fillet)
        )

    @property
    def _cap_part_base_block(self) -> cq.Workplane:
        return (
            self._get_base_block(height=self.c.shell_base_height)
            .translate(cq.Vector(0, 0, -self.c.shell_base_height))
        )

    @property
    def mold_shell(self) -> cq.Workplane:
        shell = self.cap.shell.solid
        return (
            shell
            +
            self._get_keys(enlarged_by_clearance=True)
            +
            self._cap_part_base_block
        )

    @property
    def _back_part_base_block(self) -> cq.Workplane:
        return (
            self._get_base_block(height=self.block_height)
        )

    @property
    def mold_iface_no_stem(self) -> cq.Workplane:
        return self.get_mold_iface(with_stem=False)

    @property
    def mold_iface_with_stem(self) -> cq.Workplane:
        return self.get_mold_iface(with_stem=True)

    def get_mold_iface(self, with_stem: bool) -> cq.Workplane:
        cutout = self.iface.cutout
        if with_stem:
            cutout = cutout - self.iface.stem
        cutout = cutout.translate((0, 0, self.cap.shell.interface_indentation))
        return (
            (
                self._back_part_base_block
                -
                self._get_keys(enlarged_by_clearance=False)
                -
                cutout
            )
            .rotate((0, 0, 0), (1, 0, 0), 180)
            .translate(cq.Vector(0, 0, self.block_height))
        ) - self._channel_beams

    @property
    def mold_shell_box(self) -> cq.Workplane:
        height = (
            self.c.shell_base_height
            +
            self.cap.shell.z_height
            +
            self.c.shell_box_extra_height
        )
        return self._get_box(height=height).translate(cq.Vector(0, 0, -self.c.shell_base_height))

    def _get_box(self, height: float) -> cq.Workplane:
        x_len, y_len = self.base_dim
        x_len += 2 * self.c.box_clearance
        y_len += 2 * self.c.box_clearance
        x_outer = x_len + 2 * self.c.box_wall_width
        y_outer = y_len + 2 * self.c.box_wall_width
        fillet_inner = self.c.base_fillet + self.c.box_clearance
        fillet_outer = fillet_inner + self.c.box_wall_width
        return (
            cq.Workplane()
            .rect(x_outer, y_outer)
            .extrude(height)
            .edges("|Z")
            .fillet(fillet_outer)
            -
            cq.Workplane()
            .rect(x_len, y_len)
            .extrude(height)
            .edges("|Z")
            .fillet(fillet_inner)
        )

    @property
    def mold_iface_box(self) -> cq.Workplane:
        c = self.c
        h = (
            self.block_height
            +
            c.tray_distance
            +
            c.tray_height
            +
            c.tray_overheight
        )
        return self._get_box(height=h)

    @property
    def mold_iface_tray(self) -> cq.Workplane:
        wb_x = 2 * self.c.tray_over_size + self.iface.cutout_x_width
        wb_y = 2 * self.c.tray_over_size + self.iface.cutout_y_width
        wt_x = wb_x + 2 * self.c.tray_top_extra_size
        wt_y = wb_y + 2 * self.c.tray_top_extra_size
        h = self.c.tray_height
        f = self.c.tray_fillet
        bottom = cq.Sketch().rect(wb_x, wb_y).vertices().fillet(f)
        top = cq.Sketch().rect(wt_x, wt_y).vertices().fillet(f)
        return (
            (
                cq.Workplane()
                .placeSketch(
                    bottom,
                    top.moved(cq.Location(cq.Vector((0, 0, h))))
                )
                .loft()
                +
                cq.Workplane()
                .placeSketch(
                    top.moved(cq.Location(cq.Vector((0, 0, h)))),
                    top.moved(cq.Location(cq.Vector(0, 0, h + self.c.tray_overheight)))
                )
                .loft()
            )
            .translate(cq.Vector(0, 0, self.block_height + self.c.tray_distance))
            -
            self._channel_beams
        )
